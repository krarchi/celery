from celery import Celery
import os

def make_celery(app):
    os.environ.setdefault('FORKED_BY_MULTIPROCESSING', '1')
    celery = Celery(app.import_name, result_backend=app.config['CELERY_RESULT_BACKEND'],backend=app.config['CELERY_RESULT_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'],include=[app.import_name])
    #celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery