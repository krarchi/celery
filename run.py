from flask import Flask
from tasks import make_celery
from celery.schedules import crontab , schedule

flask_app = Flask(__name__)
flask_app.config.update(
    CELERY_BROKER_URL='pyamqp://guest@localhost//',
    CELERY_RESULT_BACKEND='db+mysql://root:1234@localhost/celery'
)
celery = make_celery(flask_app)
n = 0

@flask_app.route('/')
def add():
    res=send_mail.delay()
    return "Mail Sent"

@flask_app.route('/add')
def addi():
    res = add.apply_async(args=(1,2))
    return "Hello"

@celery.task(name='tasks.add')
def add(a,b):
    return a+b

@celery.task(name='tasks.send_mail')
def send_mail():
    import smtplib, ssl

    smtp_server = "smtp.gmail.com"
    port = 587  # For starttls
    sender_email = "gndtech123@gmail.com"
    password = "newnikhil"

    # Create a secure SSL context
    context = ssl.create_default_context()

    # Try to log in to server and send email
    try:
        server = smtplib.SMTP(smtp_server,port)
        server.ehlo() # Can be omitted
        server.starttls(context=context) # Secure the connection
        server.ehlo() # Can be omitted
        server.login(sender_email, password)
        server.sendmail(sender_email, "kryashu@gmail.com", "yoyo")
    except Exception as e:
        # Print any error messages to stdout
        print(e)
    finally:
        server.quit() 

celery.conf.beat_schedule = {
    'add-every-30-seconds': {
        'task': 'tasks.send_mail',
        'schedule': schedule(run_every=6)
    },
}
if __name__=='__main__':
    flask_app.run(debug=True)